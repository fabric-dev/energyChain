package controllers
//控制层 json序列化和反序列化
import (
	"encoding/json"
	"fmt"
	"energyChain/blockchain"
	"energyChain/dataStruct"
	"github.com/astaxie/beego"
	"github.com/go.uuid"
)


const badrequest int = 400
const success int = 200

type Application struct {
	beego.Controller
}
type FabricClient struct {
	Fabric *blockchain.FabricSetup
}

var client *FabricClient = &FabricClient{}
//初始化sdk client
func Init(fabric *blockchain.FabricSetup) {
	client.Fabric = fabric
}
//http请求，写回响应json格式的响应数据
func (app *Application) ResponseJson(code int, data interface{}) {
	app.Ctx.Output.Status = code
	app.Data["json"] = data
	app.ServeJSON()
}
//存证数据
//返回数据为BlockInfoResp
func (app *Application) StoreReceipt() {
	body := app.Ctx.Input.RequestBody
	fmt.Println("接受到的存证数据:")
	fmt.Println(string(body))
	var a dataStruct.DataDigest
	resp := &dataStruct.BlockInfoResp{
		Success:false,
	}
	if err := json.Unmarshal(body, &a); err != nil {
		resp.Err = fmt.Sprintf("数据格式错误")
		fmt.Println("json数据序列化失败")
		app.ResponseJson(badrequest, resp)
		return
	}
	//获取uuid
	id, _ := uuid.NewV4()
	a.KeyId=id.String()
	if a.URI == "" || a.FileHash == "" {
		resp.Err = "keyId,fileHash,URI关键字段不能为空"
		app.ResponseJson(badrequest, resp)
		return
	}
	//检查是否已经存在重复ID
	//调用blockchain/invoke.go下的方法
	/*result, err := client.Fabric.Query("query", []byte(a.KeyId))
	if err != nil {
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
	        return
	 }
	if len(result) != 0 || result != nil {
		resp.Err="数据已经上链，无需重复上链"
		app.ResponseJson(badrequest, resp)
		return
	}*/
	//检查parentID是否正确
	if a.ParentKeyId != "" {
		result, err := client.Fabric.QueryKey(a.ParentKeyId)
		if err != nil {
			resp.Err = "内部错误"
			fmt.Println("querykey 内部错误")
			app.ResponseJson(badrequest, resp)
			return
		}
		if len(result) == 0 || result == nil {
			resp.Err = "parentId不存在"
			app.ResponseJson(badrequest, resp)
			return
		}
	}
	dataByte,_:=json.Marshal(a)
	txid, err := client.Fabric.InvokeData(dataByte)
	if err != nil {
		resp.Err = fmt.Sprintf("store err:, %v\n", err)
		app.ResponseJson(badrequest, resp)
		return
	}
	resp.Success =true
	resp.TxId = string(txid)
	//获取区块头信息
	err = client.Fabric.GetBlockHeaderInfo(resp, txid)
	if err != nil {
		resp.Success = false
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
		return
	}
	resp.KeyId=a.KeyId
	app.ResponseJson(success, resp)
}
//GET存证以后的数据
func (app *Application) LoadReceipt() {
	body := app.Ctx.Input.RequestBody
	println("json:", string(body))
	var req dataStruct.VerifyRequest
	resp := dataStruct.Response{}
	resp.Success=false
	if err := json.Unmarshal(body, &req); err != nil {
		resp.Err = fmt.Sprintf("数据格式错误")
		app.ResponseJson(badrequest, resp)
		return
	}
	if req.KeyId == "" {
		errInfo := fmt.Sprintf("keyId不能为空")
		resp.Err = errInfo
		app.ResponseJson(badrequest, resp)
		return
	}
	var ret dataStruct.TracedDataDigest
	result, err := client.Fabric.QueryKey(req.KeyId)
	if err != nil {
		resp.Err = "内部错误"
		fmt.Println("querykey 内部错误")
		app.ResponseJson(badrequest, resp)
		return
	}
	if len(result) == 0 || result == nil {
		resp.Err = "keyId不存在"
		app.ResponseJson(badrequest, resp)
		return
	}
	for {
		var data dataStruct.DataDigest
		dataByte, err := client.Fabric.QueryKey(req.KeyId)
		err = json.Unmarshal(dataByte, &data)
		if err != nil {
			fmt.Println("loadreceipt,内部反序列化结构体出错")
			resp.Err = "内部出错"
			app.ResponseJson(badrequest, resp)
			return
		}
		ret.TracedList = append(ret.TracedList, data)
		if data.ParentKeyId == "" {
			break
		}
		req.KeyId = data.ParentKeyId
	}
	app.ResponseJson(success, ret)
}
//验证文件节点hash值
func (app *Application) VerifyFile() {
	body := app.Ctx.Input.RequestBody
	println("json:", string(body))
	var req dataStruct.VerifyRequest
	//返回值
	resp := dataStruct.Response{}
	resp.Success=false
	if err := json.Unmarshal(body, &req); err != nil {
		resp.Err = fmt.Sprintf("数据格式错误")
		app.ResponseJson(badrequest, resp)
		return
	}
	var dataDigest dataStruct.DataDigest
	result, err := client.Fabric.Query("query", []byte(req.KeyId))
	if err != nil {
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
		return
	}
	if len(result) == 0 || result == nil {
		resp.Err = "keyId不存在"
		app.ResponseJson(badrequest, resp)
		return
	}
	errUnmarshal := json.Unmarshal(result, &dataDigest)
	if errUnmarshal != nil {
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
		return
	}

	fmt.Println("verifyFile get data:", string(result))
	if dataDigest.FileHash != req.FileHash {
		resp.Success=false
		resp.Err = "Hash验证不一致"
		app.ResponseJson(badrequest, resp)
		return
	}
	resp.Success=true
	resp.Err = ""
	app.ResponseJson(success, resp)
}
//验证链
func (app *Application) VerifyChain() {
	body := app.Ctx.Input.RequestBody
	println("request json:", string(body))
	var req dataStruct.Request
	resp := dataStruct.ResponseVerify{}
	resp.Success = false
	if err := json.Unmarshal(body, &req); err != nil {
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
		return
	}
	keyId := req.KeyId
	for i := len(keyId) - 1; i >= 0; i-- {
		var data dataStruct.DataDigest
		dataByte, err := client.Fabric.QueryKey(keyId[i])
		if len(dataByte) == 0 {
			resp.Err = "节点验证失败"
			tmp := dataStruct.MismatchedSet{
				Msg:   "节点不存在",
				KeyId: keyId[i],
			}
			resp.Set = append(resp.Set, tmp)
			continue
		}
		err = json.Unmarshal(dataByte, &data)
		if err != nil {
			fmt.Println("内部反序列化结构体出错")
			resp.Err = "内部出错"
			app.ResponseJson(badrequest, resp)
			return
		}

		if i > 0 && data.ParentKeyId != keyId[i-1] {
			resp.Err="节点验证失败"
			s := fmt.Sprintf("%s 父节点验证失败", data.KeyId)
			tmp := dataStruct.MismatchedSet{
				Msg:   s,
				KeyId: keyId[i],
			}
			resp.Set = append(resp.Set, tmp)
		}
	}
	if  len(resp.Set)==0{
		resp.Success=true
	}
	app.ResponseJson(success, resp)
}
func (app *Application) Default(){
	app.ResponseJson(badrequest,"访问路径错误")
}
//验证整棵树的Hash值
func (app *Application) VerifyTree() {
	body := app.Ctx.Input.RequestBody
	println("json:", string(body))
	var req dataStruct.ChainSet
	resp := &dataStruct.ResponseVerify{}
	resp.Success = false
	if err := json.Unmarshal(body, &req); err != nil {
		resp.Err = "内部出错"
		app.ResponseJson(badrequest, resp)
		return
	}
	if len(req.KeySet) == 0 {
		resp.Err = "数据为空"
		app.ResponseJson(badrequest, resp)
	}
	rootkey := req.KeySet[0][0]
	for _, keyid := range req.KeySet {
		if len(keyid) == 0 {
			continue
		}
		if keyid[0] != rootkey {
			s := fmt.Sprintf("%s 不一致", keyid[0])
			resp.Err = "节点验证失败"
			tmp := dataStruct.MismatchedSet{
				Msg:   s,
				KeyId: keyid[0],
			}
			resp.Set = append(resp.Set, tmp)
			continue
		}
		client.Fabric.VerifyChain(keyid, resp)
	}
	if resp.Err==""{	
		resp.Success = true
	}
	if len(resp.Set)==0{
		resp.Success=true
	}	
	app.ResponseJson(success, resp)

}

