package blockchain

import (
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/event"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/ledger"
	mspclient "github.com/hyperledger/fabric-sdk-go/pkg/client/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	packager "github.com/hyperledger/fabric-sdk-go/pkg/fab/ccpackager/gopackager"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/common/cauthdsl"
	"github.com/pkg/errors"
)

// Fabric sdk 
type FabricSetup struct {
	//配置文件config.yaml路径
	ConfigFile      string
	OrgID           string
	OrdererID       string
	ChannelID       string
	ChainCodeID     string
	initialized     bool
	ChannelConfig   string
	ChaincodeGoPath string
	ChaincodePath   string
	OrgAdmin        string
	OrgName         string
	UserName        string
	//通道客户端，发起交易
	//目标连接端口
	TargetEndpoint  string //"grpcs://××.××.××.××:7051"
	client          *channel.Client
	//资源管理客户端 管理通道的创建和更新
	admin           *resmgmt.Client
	sdk             *fabsdk.FabricSDK
	//发起一笔交易注册一个事件
	event           *event.Client
	//账本客户端 获取区块信息
	ledgerClient    *ledger.Client
}
func (setup *FabricSetup) CreateChannelClient() error{
        if setup.initialized {
                        return errors.New("sdk already initialized")
        }

        // 从配置文件中实例化sdk
        sdk, err := fabsdk.New(config.FromFile(setup.ConfigFile))
        if err != nil {
                        return errors.WithMessage(err, "failed to create SDK")
        }
        setup.sdk = sdk
        fmt.Println("SDK created")
	//获取通道的上下文信息
        clientContext := setup.sdk.ChannelContext(setup.ChannelID, fabsdk.WithUser("Admin"), fabsdk.WithOrg(setup.OrgName) )
       //生成一个通道客户端
	setup.client, err = channel.New(clientContext)
        if err != nil {
                        return errors.WithMessage(err, "failed to create new channel client")
        }
        fmt.Println("Channel client created")
        //创建 ledger client
        setup.ledgerClient, err = ledger.New(clientContext)
        if err != nil {
                        return errors.WithMessage(err, "failed to create new ledger client")
        }
        // 创建一个通道事件客户端，可以获取通道事件（一般一笔交易注册一个事件）
        setup.event, err = event.New(clientContext)
        if err != nil {
                        return errors.WithMessage(err, "failed to create new event client")
        }
        fmt.Println("Event client created")
	return nil
}

func (setup *FabricSetup) Initialize() error {

	if setup.initialized {
		return errors.New("sdk already initialized")
	}

	// 根据配置文件生成一个sdk客户端
	sdk, err := fabsdk.New(config.FromFile(setup.ConfigFile))
	if err != nil {
		return errors.WithMessage(err, "failed to create SDK")
	}
	setup.sdk = sdk
	fmt.Println("SDK created")

	//生成一个资源管理客户端负责channel的创建和更新
	resourceManagerClientContext := setup.sdk.Context(fabsdk.WithUser(setup.OrgAdmin), fabsdk.WithOrg(setup.OrgName))
	if err != nil {
		return errors.WithMessage(err, "failed to load Admin identity")
	}
	resMgmtClient, err := resmgmt.New(resourceManagerClientContext)
	if err != nil {
		return errors.WithMessage(err, "failed to create channel management client from Admin identity")
	}
	setup.admin = resMgmtClient
	fmt.Println("Ressource management client created")

	// 验证用户客户端身份
	//验证通过以后允许加入通道
	mspClient, err := mspclient.New(sdk.Context(), mspclient.WithOrg(setup.OrgName))
	if err != nil {
		return errors.WithMessage(err, "failed to create MSP client")
	}
	adminIdentity, err := mspClient.GetSigningIdentity(setup.OrgAdmin)
	if err != nil {
		return errors.WithMessage(err, "failed to get admin signing identity")
	}
	req := resmgmt.SaveChannelRequest{ChannelID: setup.ChannelID, ChannelConfigPath: setup.ChannelConfig, SigningIdentities: []msp.SigningIdentity{adminIdentity}}
	txID, err := setup.admin.SaveChannel(req, resmgmt.WithOrdererEndpoint(setup.OrdererID))
	if err != nil || txID.TransactionID == "" {
		return errors.WithMessage(err, "failed to save channel")
	}
	fmt.Println("Channel created")

	//将admin用户加入到创建的通道（实际上就是将当前组织加入通道）
	if err = setup.admin.JoinChannel(setup.ChannelID, resmgmt.WithRetry(retry.DefaultResMgmtOpts), resmgmt.WithOrdererEndpoint(setup.OrdererID)); err != nil {
		return errors.WithMessage(err, "failed to make admin join channel")
	}
	fmt.Println("Channel joined")

	fmt.Println("Initialization Successful")
	setup.initialized = true
	return nil
}

func (setup *FabricSetup) InstallAndInstantiateCC() error {

	//生成一个链码
	ccPkg, err := packager.NewCCPackage(setup.ChaincodePath, setup.ChaincodeGoPath)
	if err != nil {
		return errors.WithMessage(err, "failed to create chaincode package")
	}
	fmt.Println("ccPkg created")

	// 安装链码
	installCCReq := resmgmt.InstallCCRequest{Name: setup.ChainCodeID, Path: setup.ChaincodePath, Version: "0", Package: ccPkg}
	_, err = setup.admin.InstallCC(installCCReq, resmgmt.WithRetry(retry.DefaultResMgmtOpts))
	if err != nil {
		return errors.WithMessage(err, "failed to install chaincode")
	}
	fmt.Println("Chaincode installed")

	// 设置链码实例化策略（链码的实例化相当于一笔交易），需要设置背书策略
	ccPolicy := cauthdsl.SignedByAnyMember([]string{"Org1MSP"})

	resp, err := setup.admin.InstantiateCC(setup.ChannelID, resmgmt.InstantiateCCRequest{Name: setup.ChainCodeID, Path: setup.ChaincodeGoPath, Version: "0", Args: [][]byte{[]byte("init")}, Policy: ccPolicy})
	if err != nil || resp.TransactionID == "" {
		return errors.WithMessage(err, "failed to instantiate the chaincode")
	}
	fmt.Println("Chaincode instantiated")
	fmt.Println("*********query and execute transactions*********")
	clientContext := setup.sdk.ChannelContext(setup.ChannelID, fabsdk.WithUser(setup.UserName))
	setup.client, err = channel.New(clientContext)
	if err != nil {
		return errors.WithMessage(err, "failed to create new channel client")
	}
	fmt.Println("Channel client created")
	//create ledger client
	setup.ledgerClient, err = ledger.New(clientContext)
	if err != nil {
		return errors.WithMessage(err, "failed to create new ledger client")
	}
	//创建事件客户端
	setup.event, err = event.New(clientContext)
	if err != nil {
		return errors.WithMessage(err, "failed to create new event client")
	}
	fmt.Println("Event client created")

	fmt.Println("Chaincode Installation & Instantiation Successful")
	return nil
}

func (setup *FabricSetup) CloseSDK() {
	setup.sdk.Close()
}
