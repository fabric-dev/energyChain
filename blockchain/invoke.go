package blockchain

import (
	"errors"
	"fmt"
	"time"
	 "github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
)

// 发起一笔存证数据交易
func (setup *FabricSetup) InvokeData(value []byte) ([]byte, error) {
	var args []string
	args = append(args, "invoke")
	args = append(args, "storeDigest")
	eventID := "eventInvoke"
	transientDataMap := make(map[string][]byte)
	transientDataMap["result"] = []byte("Transient data :add key-value ")

	reg, notifier, err := setup.event.RegisterChaincodeEvent(setup.ChainCodeID, eventID)
	if err != nil {
		fmt.Println(err)
		return nil, errors.New("内部出错")
	}
	defer setup.event.Unregister(reg)
	// 向链码发起交易请求
	fmt.Println("create a store request.....")
	response, err := setup.client.Execute(channel.Request{ChaincodeID: setup.ChainCodeID, Fcn: args[0], Args: [][]byte{[]byte(args[1]), value}, TransientMap: transientDataMap},
		channel.WithRetry(retry.DefaultChannelOpts),
                channel.WithTargetEndpoints(setup.TargetEndpoint),
	)
	if err != nil {
		fmt.Printf("向链码发起存储请求时出错: %v\n", err)
		return nil, errors.New("内部出错")
	}

	// 等待提交结果
	select {
	case ccEvent := <-notifier:
		fmt.Printf("Received CC event: %v\n", ccEvent)
	case <-time.After(time.Second * 20):
		fmt.Printf("did NOT receive CC event for eventId(%s)\n", eventID)
		return nil, errors.New("内部出错")
	}

	return []byte(response.TransactionID), nil
}
