package main

import (
	"fmt"
	"energyChain/blockchain"
	"energyChain/controllers"
	_ "energyChain/routers"
	"os"

	"github.com/astaxie/beego"
)

func main() {
	// 定义fabric sdk 的初始化参数
	fSetup := blockchain.FabricSetup{
		// 排序节点ID
		OrdererID: "orderer1.example.com",

		// 通道名字
		ChannelID:     "mychannel",
		ChannelConfig: os.Getenv("GOPATH") + "/src/github.com/hyperledger/fabric/scripts/fabric-samples/test_nodes/channel-artifacts/channel.tx",

		// 链码ID（第一次安装链码时可以任意指定）
		ChainCodeID:     "mycc",
		ChaincodeGoPath: os.Getenv("GOPATH"),
		ChaincodePath:   "github.com/energyChain/chaincode/",
		OrgAdmin:        "Admin",
		OrgName:         "org1",
		//配置文件
		ConfigFile:      "config.yaml",
		UserName: "User1",
		TargetEndpoint:"grpcs://10.108.26.227:7051",
	}

	//用fabric创建通道\加入peer节点到通道\安装链码\实例化链码
	// 初始化SDK，创建SDK 客户端
/*	err := fSetup.Initialize()
	if err != nil {
		fmt.Printf("Unable to initialize the Fabric SDK: %v\n", err)
		return
	}
	// Close SDK
	defer fSetup.CloseSDK()

	// 安装并实例化链码
	err = fSetup.InstallAndInstantiateCC()
	if err != nil {
		fmt.Printf("Unable to install and instantiate the chaincode: %v\n", err)
		return
	}
	//创建channel client 客户端(channel已经被创建并保存到prodution中，需要重新生成)
*/
	err := fSetup.CreateChannelClient()
        if err != nil {
                fmt.Printf("Unable to create Fabric SDK client: %v\n", err)
                return
        }

	controllers.Init(&fSetup)
	beego.Run()
}
