package routers
//路由层
import (
	"energyChain/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/verifyChain", &controllers.Application{}, "post:VerifyChain")
	beego.Router("/verifyFile", &controllers.Application{}, "post:VerifyFile")
	beego.Router("/loadReceipt", &controllers.Application{}, "post:LoadReceipt")
	beego.Router("/storeReceipt", &controllers.Application{}, "post:StoreReceipt")
	beego.Router("/verifyTree", &controllers.Application{}, "post:VerifyTree")
	beego.Router("/", &controllers.Application{}, "get,post:Default")

}
